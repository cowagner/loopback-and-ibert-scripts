#!/usr/bin/env python                                                                                                                                              

import os,sys
import argparse
import shutil
import time

now = time.strftime("%Y_%m_%d_%Hh%Mm%Ss")

parser = argparse.ArgumentParser()
parser.add_argument ("--programmer", '-p', default="/afs/cern.ch/work/n/nswdaq/public/cooper/progTP_AT_test.sh", help="path to config file to program FPGAs")
parser.add_argument ("--felix", '-f', default="/afs/cern.ch/work/n/nswdaq/public/felix-sw/software_20200203/software/x86_64-centos7-gcc8-opt/felixcore/felixcore", help="filepath to felixcore connection")
parser.add_argument ("--opc", '-o', default="acceptance-tests/configFiles/TP_flipped.xml", help="filepath to opc .xml file")
parser.add_argument ("--elinks", '-e', default="/afs/cern.ch/work/n/nswdaq/public/cooper/loopback_elinks.sh", help="filepath to elinks config file")
parser.add_argument ("--test", '-t', default="/afs/cern.ch/work/n/nswdaq/public/cooper/loopback_tests_v1.sh", help="path to file with tests to run")
args = parser.parse_args()


def main():
    program_tps()
    start_felixcore_and_scaserver()
    run_tests()
    stop_felixcore_and_scaserver()
    print("Done! (^.^)")


def program_tps():

    os.system("""                                                                                                                                                 
    source %s                                                                                                                                    
    """ % args.programmer)                                                                                                                                                          

    print("Done: TP programmed")

def start_felixcore_and_scaserver():
    
    os.system("""                                                                                                                                                                                          
#    python ./flxopc.py --quiet --uid {0} &                                                                                                                                                                 
     python /afs/cern.ch/work/n/nswdaq/public/cooper/flxopc.py --flx %s -o %s --quiet --uid {0} &                                                                                                                                                                                                     
                                                                                                                                                                                                    
    flx-info gbt                                                                                                                                                                                            
    """.format(now) % (args.felix, args.opc))




    while not os.path.isfile("/tmp/flxopc/{0}/ok".format(uid)):
        time.sleep(1)
        
    print("Done: felixcore and opc connected")


def stop_felixcore_and_scaserver():

    os.system("""
    touch /tmp/flxopc/{0}/kill
    """.format(now))

    print("Done: felixcore and opc dead")


def run_tests():

    time.sleep(10)
    os.system("""

    %s

    %s

     """ % (args.elinks, args.test))

    print("Done: tests run")


if __name__ == "__main__":
    main()
