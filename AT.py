#!/usr/bin/env python                                                                                                                                              

import os,sys
import argparse
import shutil
import time

now = time.strftime("%Y_%m_%d_%Hh%Mm%Ss")

def main():
    program_tps()
    start_felixcore_and_scaserver()
    run_tests()
    stop_felixcore_and_scaserver()
    print("Done! (^.^)")


def program_tps():

    #os.system("""                                                                                                                                                 
    #source ./progTP_AT_test.sh                                                                                                                                    
    #""")                                                                                                                                                          

    print("Done: TP programmed")

def start_felixcore_and_scaserver():
    
    os.system("""                          
#    python ./flxopc.py --quiet --uid {0} &

    python ./flxopc.py --flx /afs/cern.ch/work/n/nswdaq/public/felix-sw/software_20200203/software/x86_64-centos7-gcc8-opt/felixcore/felixcore -o acceptance-tests/configFiles/TP_flipped.xml --quiet --uid {0} &
                                                                                                                                                                   
    flx-info gbt                                                                                                                                                   
    """.format(now))

    while not os.path.isfile("/tmp/flxopc/{0}/ok".format(uid)):
        time.sleep(1)
        
    print("Done: felixcore and opc connected")


def stop_felixcore_and_scaserver():

    os.system("""
    touch /tmp/flxopc/{0}/kill
    """.format(now))

    print("Done: felixcore and opc dead")


def run_tests():

    time.sleep(10)
    os.system("""

    netio_cat subscribe -H pcatlnswfelix01 -p 12350 -e raw -f plain -t 417 &
    netio_cat subscribe -H pcatlnswfelix01 -p 12350 -e raw -f plain -t 481 &

    ./acceptance-tests/scripts/x86_64-centos7-gcc8-opt/NSWConfiguration/mmtp_loopback_test -o pcatlnswfelix01.cern.ch:48020 -s NSW_TrigProc_MM_0.I2C_0.bus0 -d acceptance-tests/scripts/playbackData/l1aConfig.txt
    ./acceptance-tests/scripts/x86_64-centos7-gcc8-opt/NSWConfiguration/mmtp_loopback_test -o pcatlnswfelix01.cern.ch:48020 -s NSW_TrigProc_MM_1.I2C_0.bus0 -d acceptance-tests/scripts/playbackData/l1aConfig.txt

    ./acceptance-tests/scripts/x86_64-centos7-gcc8-opt/NSWConfiguration/mmtp_loopback_test -o pcatlnswfelix01.cern.ch:48020 -s NSW_TrigProc_ARTEmu_0.I2C_0.bus0 -d acceptance-tests/scripts/playbackData/l1aSingle_1.txt -n 10
    ./acceptance-tests/scripts/x86_64-centos7-gcc8-opt/NSWConfiguration/mmtp_loopback_test -o pcatlnswfelix01.cern.ch:48020 -s NSW_TrigProc_ARTEmu_1.I2C_0.bus0 -d acceptance-tests/scripts/playbackData/l1aSingle_1.txt -n 10


     """)

    print("Done: tests run")


if __name__ == "__main__":
    main()
