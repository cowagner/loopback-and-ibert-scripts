#!/usr/bin/env python                                                                                                                                              

import os,sys
import argparse
import shutil


parser = argparse.ArgumentParser()
parser.add_argument ("--carrier",'-c', action='store_true', default=False, help="program carrier")
parser.add_argument ("--mezzanine",'-m', action='store_true', default=False, help="program mezzanine")
parser.add_argument ("--programmer",'-p', default="210357AC3AF9A", help="programmer ID")
parser.add_argument ("--whichFPGAs",'-f', default="0,1,2,3", help="comma separated list of FPGAs 0,1,2,3")
parser.add_argument ("--bitfile",'-b', default="BITFILENAME", help=".bit file for FPGA")
parser.add_argument ("--server",'-s', default="3122", help="localhost server connection")
parser.add_argument ("--filepath", '-file', default="sample.tcl", help="filepath to .tcl file for vivado")
parser.add_argument ("--flip", '-flip', action='store_true', default=False, help="flips mezz programming")
args = parser.parse_args()

whichFPGAs = args.whichFPGAs.split(",")
whichFPGAs = [int(x) for x in whichFPGAs]

thisDirName = os.path.dirname(os.path.realpath(__file__))

### generates tcl file for NORMAL FPGA programming ###

def tclgen():

    f= open("sample.tcl","w+")
    f.write("open_hw\n")
    f.write("connect_hw_server -url localhost:%s\n" %(args.server))
    f.write("#left FPGA\n")
    f.write("#current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210357A7D2D4A]\n")
    f.write("#set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/210357A7D2D4A]\n")
    f.write("#right FPGA\n")
    f.write("current_hw_target [get_hw_targets */xilinx_tcf/Digilent/%s]\n" % (args.programmer))
    f.write("set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/%s]\n" % (args.programmer))
    f.write("open_hw_target\n")
    f.write("\n")
    f.write("##########################\n")
    f.write("\n")
    f.write("\n")


    for i in whichFPGAs:
        f.write("current_hw_device [lindex [get_hw_devices] %s]\n" % i)
        f.write("refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] %s]\n"  % i)
        f.write("\n")
        f.write("set_property PROGRAM.FILE {%s.bit} [lindex [get_hw_devices]" % (args.bitfile[:-4]))
        f.write(" %s]\n" % i)
        f.write("set_property PROBES.FILE  {%s.ltx} [lindex [get_hw_devices]" % (args.bitfile[:-4]))
        f.write(" %s]\n" % i)
        f.write("\n")
        f.write("program_hw_device [lindex [get_hw_devices] %s]\n" % i)
        f.write("refresh_hw_device [lindex [get_hw_devices] %s]\n" % i)
        f.write("\n")
        f.write("##########################\n")
        f.write("\n")

    if args.carrier:
        f.write("""
        set_property OUTPUT_VALUE 1 [get_hw_probes gbtex/rxPolarity -of_objects [get_hw_vios -of_objects [get_hw_devices xcku060_0] -filter {CELL_NAME=~"gbtex/vio"}]]
        commit_hw_vio [get_hw_probes {gbtex/rxPolarity} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku060_0] -filter {CELL_NAME=~"gbtex/vio"}]]
        """)

    return



### generates FLIPPED tcl file for mezz FPGAs ###

def tclgenflip():

    f= open("sample.tcl","w+")
    f.write("open_hw\n")
    f.write("connect_hw_server -url localhost:3122\n")
    f.write("#left FPGA\n")
    f.write("#current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210357A7D2D4A]\n")
    f.write("#set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/210357A7D2D4A]\n")
    f.write("#right FPGA\n")
    f.write("current_hw_target [get_hw_targets */xilinx_tcf/Digilent/%s]\n" % (args.programmer))
    f.write("set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/%s]\n" % (args.programmer))
    f.write("open_hw_target\n")
    f.write("\n")
    f.write("##########################\n")
    f.write("\n")
    f.write("\n")


    for i in whichFPGAs:
        if i == 1:
            j = 4
        if i == 2:
            j = 5
        if i == 4:
            j = 1
        if i == 5:
            j = 2
        f.write("current_hw_device [lindex [get_hw_devices] %s]\n" % j)
        f.write("refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] %s]\n"  % j)
        f.write("\n")
        f.write("set_property PROGRAM.FILE {%s.bit} [lindex [get_hw_devices]" % args.bitfile)
        f.write(" %s]\n" % j)
        f.write("set_property PROBES.FILE  {%s.ltx} [lindex [get_hw_devices]" % args.bitfile)
        f.write(" %s]\n" % j)
        f.write("\n")
        f.write("program_hw_device [lindex [get_hw_devices] %s]\n" % j)
        f.write("refresh_hw_device [lindex [get_hw_devices] %s]\n" % j)
        f.write("\n")
        f.write("##########################\n")
        f.write("\n")

    if args.carrier:
        f.write("""
        set_property OUTPUT_VALUE 1 [get_hw_probes gbtex/rxPolarity -of_objects [get_hw_vios -of_objects [get_hw_devices xcku060_0] -filter {CELL_NAME=~"gbtex/vio"}]]
        commit_hw_vio [get_hw_probes {gbtex/rxPolarity} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku060_0] -filter {CELL_NAME=~"gbtex/vio"}]]
        """)

    return




### Code to run from command line ###

if args.filepath == "sample.tcl":
    tclgen()

if args.flip == True:
    tclgenflip()

val = os.system("""                                                                                                                                                                                     
      source /opt/Xilinx/Vivado/2018.3/settings64.sh                                                                                                                                                      #      vivado -mode batch -notrace -nolog -nojournal -quiet -source %s                                                                                                                             
      """ % args.filepath) 

if val != 0:
    print("Warning: ERROR Check code below")
    print(val)
    sys.exit(val)

