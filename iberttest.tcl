#refreshing mezzanines
refresh_hw_device [lindex [get_hw_devices xc7vx690t_*] 0]


#CF0-MEZZF1

set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y4/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y4/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y5/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y5/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y6/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y6/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y7/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_225/MGT_X1Y7/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]

#MEZZF1-CFO does not require Rx Polarity invert

#CF0-MEZZF2

set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y0/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y0/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y1/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y1/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y2/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y2/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y3/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y3/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]

#MEZZF2-CF0 does not require Rx Polarity invert

#CF3-MEZZF4

set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y0/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y0/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y1/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y1/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y2/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y2/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y3/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/0_1_0_5/IBERT/Quad_224/MGT_X1Y3/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]

#MEZZF4-CF3 does not require Rx polarity invert

#CF3-MEZZF5

set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y0/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y0/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y28/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y1/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y1/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y29/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y2/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y2/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y30/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y3/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/3_1_0_5/IBERT/Quad_224/MGT_X1Y3/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_217/MGT_X0Y31/RX}]

#MEZZF5-CF3 does not require Rx polarity invert

#Link 80 MEZZF2-MEZZF1

set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_118/MGT_X1Y32/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_118/MGT_X1Y32/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_118/MGT_X1Y32/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_118/MGT_X1Y32/RX}]

# Link 83 MEZZF2-MEZZF1 Rx ticked and Tx un-ticked
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]
set_property PORT.TXPOLARITY {0} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/2_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/1_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]

# Link 127 MEZZF4-MEZZF5 

set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/4_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/4_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]

# Link 171 MEZZF5-MEZZF4
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/4_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]
commit_hw_sio [get_hw_sio_links {pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/5_1_0/IBERT/Quad_118/MGT_X1Y35/TX->pcatlnswfelix01.cern.ch:3122/xilinx_tcf/Digilent/210357AC3ABEA/4_1_0/IBERT/Quad_118/MGT_X1Y35/RX}]

#BERT reset, TX reset, RX reset
set_property LOGIC.MGT_ERRCNT_RESET_CTRL 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
set_property LOGIC.MGT_ERRCNT_RESET_CTRL 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
set_property PORT.GTTXRESET 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
set_property PORT.GTTXRESET 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
set_property PORT.GTRXRESET 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
set_property PORT.GTRXRESET 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
refresh_hw_sio [list [get_hw_sio_linkgroups {LINKGROUP_*}] ]

set_property LOGIC.MGT_ERRCNT_RESET_CTRL 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
set_property LOGIC.MGT_ERRCNT_RESET_CTRL 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {LINKGROUP_*}]]
refresh_hw_sio [list [get_hw_sio_linkgroups {LINKGROUP_*}] ]
