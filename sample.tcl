open_hw
connect_hw_server -url localhost:3122
#left FPGA
#current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210357A7D2D4A]
#set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/210357A7D2D4A]
#right FPGA
current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210357AC3ABEA]
set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/210357AC3ABEA]
open_hw_target

##########################


current_hw_device [lindex [get_hw_devices] 4]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 4]

set_property PROGRAM.FILE {/eos/atlas/atlascerngroupdisk/det-nsw/fw/tp/acceptanceTests/bitfiles/top_horx_artem_20200318_174557_0.012ns.bit} [lindex [get_hw_devices] 4]
set_property PROBES.FILE  {/eos/atlas/atlascerngroupdisk/det-nsw/fw/tp/acceptanceTests/bitfiles/top_horx_artem_20200318_174557_0.012ns.ltx} [lindex [get_hw_devices] 4]

program_hw_device [lindex [get_hw_devices] 4]
refresh_hw_device [lindex [get_hw_devices] 4]

##########################

current_hw_device [lindex [get_hw_devices] 5]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 5]

set_property PROGRAM.FILE {/eos/atlas/atlascerngroupdisk/det-nsw/fw/tp/acceptanceTests/bitfiles/top_horx_artem_20200318_174557_0.012ns.bit} [lindex [get_hw_devices] 5]
set_property PROBES.FILE  {/eos/atlas/atlascerngroupdisk/det-nsw/fw/tp/acceptanceTests/bitfiles/top_horx_artem_20200318_174557_0.012ns.ltx} [lindex [get_hw_devices] 5]

program_hw_device [lindex [get_hw_devices] 5]
refresh_hw_device [lindex [get_hw_devices] 5]

##########################

