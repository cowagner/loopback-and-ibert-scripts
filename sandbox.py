import os,sys
import argparse
import shutil
import time


parser = argparse.ArgumentParser()
parser.add_argument ("--programmer", '-p', default="/afs/cern.ch/work/n/nswdaq/public/cooper/progTP_AT_test.sh", help="path to config file to program FPGAs")
parser.add_argument ("--felix", '-f', default="/afs/cern.ch/work/n/nswdaq/public/felix-sw/software_20200203/software/x86_64-centos7-gcc8-opt/felixcore/felixcore", help="filepath to felixcore connection")
args = parser.parse_args()



print("""Check: 1. %s    2. %s  {0}""".format(1) % (args.programmer, args.felix))
