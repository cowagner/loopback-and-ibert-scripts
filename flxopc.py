"""
Launch threads for felixcore and OPC server.

Run like:
  python flxopc.py -o "/path/to/my/opc.xml"
"""
import argparse
import itertools
import multiprocessing
import os
import sys
import time
import uuid

def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--flx",   default="felixcore",                              help="felixcore command")
    parser.add_argument("--opc",   default="/opt/OpcUaScaServer/bin/OpcUaScaServer", help="OPC server command")
    parser.add_argument("-f",      default="--data-interface eno1",                  help="felixcore args")
    parser.add_argument("-o",      default="/opt/OpcUaScaServer/bin/4l1ddc.xml",     help="OPC server args")
    parser.add_argument("--log",   default="/tmp/flxopc/",                           help="Directory for logs")
    parser.add_argument("--uid",   default=None,                                     help="Unique ID for logs")
    parser.add_argument("--quiet", action="store_true",                              help="Suppress some output")
    return parser.parse_args()

def main():

    # user inputs
    ops = options()

    # logs
    uid = ops.uid if ops.uid else uuid.uuid4().hex
    logs = os.path.join(ops.log, uid)
    os.makedirs(logs)
    print("Logs:\n %s" % (logs))
    time.sleep(1)

    # launch processes
    threads = []
    for func in [felixcore, OpcUaScaServer, watchdog]:
        proc = multiprocessing.Process(target=func, args=(logs,))
        threads.append(proc)
        proc.start()

    # user interface
    try:
        while True:
            time.sleep(1)
            if killfile_exists(logs):
                print("\n killfile exists. Exiting.\n")
                raise
            elif felixcore_problem(logs):
                print("\n felixcore problem. Exiting.\n")
                raise
            elif OpcUaScaServer_problem(logs):
                print("\n OpcUaScaServer problem. Exiting.\n")
                raise
    except KeyboardInterrupt:
        pass
    except:
        pass
    for thr in threads:
        thr.terminate()
        thr.join()

    # clean up
    kill_processes()
    top()

def felixcore(logs):
    ops = options()
    out = os.path.join(logs, "felixcore.txt")
    cmd = "%s %s 2>&1 | tee %s" % (ops.flx, ops.f, out)
    run(cmd)

def OpcUaScaServer(logs):
    while not felixcore_ready(logs):
        time.sleep(1)
    ops = options()
    out = os.path.join(logs, "OpcUaScaServer.txt")
    cmd = "%s %s 2>&1 | tee %s" % (ops.opc, ops.o, out)
    run(cmd)

def watchdog(logs):
    ops = options()
    while not OpcUaScaServer_ready(logs):
        time.sleep(1)
    print("")
    print("To pkill %s and %s, you can do either:" % (os.path.basename(ops.flx), os.path.basename(ops.opc)))
    print("  Press Ctrl+C")
    print("  touch %s" % (killfile_name(logs)))
    print("")
    # https://stackoverflow.com/questions/4995733/
    spinner = itertools.cycle(['-', '/', '|', '\\'])
    if not ops.quiet:
        while True:
            sys.stdout.write(next(spinner))
            sys.stdout.flush()
            sys.stdout.write('\b')
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                return

def felixcore_ready(logs):
    ready = "FelixCore Up and Running"
    return count(os.path.join(logs, "felixcore.txt"), ready)

def OpcUaScaServer_ready(logs):
    ready = "Press CTRL-C to shutdown server"
    return count(os.path.join(logs, "OpcUaScaServer.txt"), ready)

def felixcore_problem(logs):
    log = os.path.join(logs, "felixcore.txt")
    problems = []
    for (problem, threshold) in problems:
        if count(os.path.join(logs, log), problem) >= threshold:
            print("\nProblem: Too many `%s` in %s\n" % (problem, log))
            return True
    return False

def OpcUaScaServer_problem(logs):
    log = os.path.join(logs, "OpcUaScaServer.txt")
    problems = [ ["#deferred SCAs", 5],
             ]
    for (problem, threshold) in problems:
        if count(os.path.join(logs, log), problem) >= threshold:
            print("\nProblem: Too many `%s` in %s\n" % (problem, log))
            return True
    return False

def killfile_name(logs):
    return logs + ".kill"

def killfile_exists(logs):
    return os.path.isfile(killfile_name(logs))

def count(filename, string):
    if not os.path.isfile(filename):
        return 0
    lines = open(filename).readlines()
    total = 0
    for line in lines:
        if string in line:
            total += 1
    return total

def kill_processes():
    # allow things time to die
    time.sleep(1)
    ops = options()
    for killme in [os.path.basename(ops.opc),
                   os.path.basename(ops.flx),
                   ]:
        cmd = "pkill -9 %s" % (killme)
        run(cmd, crash=False)
    time.sleep(1)

def top():
    cmd = "top -u %s -b -n 1" % (os.environ["USER"])
    run(cmd, crash=False)

def run(cmd, crash=True):
    print("Running: \n %s\n" % (cmd))
    time.sleep(1)
    result = os.system(cmd)
    if crash and not result==0:
        fatal("Command `%s` failed with status=%s" % (cmd, result))

def fatal(msg):
    import sys
    kill_processes()
    sys.exit("Fatal error: %s. Killed felixcore and OpcUaScaServer to be safe." % (msg))

if __name__ == "__main__":
    main()
